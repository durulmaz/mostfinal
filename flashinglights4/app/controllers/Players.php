<?php
  class Players extends Controller {
    public function __construct(){
        if(!isLoggedIn()){
            redirect('users/login');
        }
       $this->playerModel = $this->model('Player');
       $this->userModel = $this->model('User');
       $this->teamModel = $this->model('Team');
        
       

    }

    public function index(){
        
        $data = [
            'title' => 'SportGeek football',
            'description' => 'You sport? you geek? you SportGeek!'
                ];

        $this->view('competition/index', $data);
      
    }


    // een controller heeft een function nodig, dit moet je toevoegen voor elke pagina die je maakt. 
    public function player(){
        

        $player = $this->playerModel->getPlayers();
        $data = [
            'players' => $player
        ];
        $this->view('competition/player/player', $data);
      //  $this->view('competition/player/insertingone', $data);  --> niet nodig hier




    }




   

public function insertingone(){
    
    // dit zal checken of het om een POST actie gaat 
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
// het formulier uitvoeren

//Sanitize post data 
$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

$data =[
    'FirstName' => trim($_POST['FirstName']),
    'LastName' => trim($_POST['LastName']),
    'Email' => trim($_POST['Email']),
    'Address1' => trim($_POST['Address1']),
    'Address2' =>trim($_POST['Address2']),
    'PostalCode' =>trim($_POST['PostalCode']),
    'City' =>trim($_POST['City']),
    'Country' =>trim($_POST['Country']),
    'Phone' =>trim($_POST['Phone']),
    'Birthday' =>trim($_POST['Birthday']),
    'TeamId' => (int)$_POST['TeamId'],
    'FirstName_err' => '',
    'LastName_err' =>'',
    'Email_err' => '',
    'Address1_err' =>'',
    'Address_err' => '',
    'PostalCode_err' => '',
    'City_err' => '',
    'Country_err' => '',
    'Phone_err' =>'',
    'Birthday_err' => ''
];

// validate firstname
if(empty($data['FirstName'])){
    $data['FirstName_err'] = 'Please enter first name';
}
// validate lastname
if(empty($data['LastName'])){
    $data['LastName_err'] = 'Please enter last name';
}
// validate email
if(empty($data['Email'])){
    $data['Email_err'] = 'Please enter email address';
}
// validate address1
if(empty($data['Address1'])){
    $data['Address1_err'] = 'Please enter address';
}
// validate address2
if(empty($data['Address2'])){
    $data['Address2_err'] = 'Please enter address';
}
// validate postalcode
if(empty($data['PostalCode'])){
    $data['PostalCode_err'] = 'Please enter postal code';
}
// validate city
if(empty($data['City'])){
    $data['City_err'] = 'Please enter city';
}
// validate country
if(empty($data['Country'])){
    $data['Country_err'] = 'Please enter Country';
}
// validate Phone
if(empty($data['Phone'])){
    $data['Phone_err'] = 'Please enter phone';
}
// validate birthday
if(empty($data['Birthday'])){
    $data['Birthday_err'] = 'Please enter birthday';
}

// zorg dat alle error's leeg zijn
if(empty($data['FirstName_err']) && empty($data['LastName_err']) && empty($data['Email_err']) && 
empty($data['Address1_err']) && empty($data['Address2_err']) && empty($data['PostalCode_err']) && 
empty($data['City_err']) && empty($data['Country_err']) && empty($data['Phone_err']) && empty($data['Birthday_err']))
{
    // validated
   // die('SUCCESS');

if($this->playerModel->insertingone($data)){
    flash('register_success', 'The player has been added');
    redirect('players/player');

    // flash('post_message', 'Player added');
    //                         redirect('player'); 
} else{
    die('Something went wrong');
}





} else{
    // load view with errors
    $this->view('competition/player/insertingone', $data);

}


 } else {
        // het formulier laden ( test hieronder of de url football/insertplayer werkt met de echo)
        //echo 'laadt de spelerslijst';
//initialiseer data 

//laadt de spelerlijst en initialiseer de data
$teams = $this->teamModel->getTeams();
$data =[
    'FirstName' =>  '',
    'LastName' =>  '',
    'Email' =>  '',
    'Address1' =>  '',
    'Address2' => '',
    'PostalCode' => '',
    'City' => '',
    'Country' => '',
    'Phone' => '',
    'Birthday' => '',
    'Id' => '',
    'TeamId' => '',
    'teams' => $teams,
    'FirstName_err' => '',
    'LastName_err' =>'',
    'Email_err' => '',
    'Address1_err' =>'',
    'Address_err' => '',
    'PostalCode_err' => '',
    'City_err' => '',
    'Country_err' => '',
    'Phone_err' =>'',
    'Birthday_err' => ''
    

  
 
];

// load view
$this->view('competition/player/insertingone', $data);





    



// validate email 
if(empty($data['email'])){
    $data['email_err'] = 'Please enter email';
} else {
// check email
if($this->userModel->findUserByEmail($data['email'])){
    $data['email_err'] = 'There is already a user registered with this e-mail address.';
}
}

//validate first Name 
if(empty($data['firstname'])){
$data['firstname_err'] = 'Please enter first name';
}

//validate Last Name 
if(empty($data['lastname'])){
$data['lastname_err'] = 'Please enter last name';
}
//validate Age 
if(empty($data['age'])){
$data['age_err'] = 'Please enter age';
}

//validate Location 
if(empty($data['location'])){
$data['location_err'] = 'Please enter location';
}


    }
}



public function showPlayer($id){

    $player = $this->playerModel->getPlayerById($id);
    $team = $this->teamModel->getTeamById($player->TeamId);
    

    $data = [
        'player' => $player,
        'team' => $team
        

    ];

$this->view('competition/player/showPlayer', $data);

}






public function editPlayer($id){
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        // SANITIZE POST array
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $data = [

            'Id' => $id,
            'FirstName' => trim($_POST['FirstName']),
            'LastName' => trim($_POST['LastName']),
            'Email' => trim($_POST['Email']),
            'Address1' => trim($_POST['Address1']),
            'Address2' => trim($_POST['Address2']),
            'PostalCode' => trim($_POST['PostalCode']),
            'City' => trim($_POST['City']),
            'Country' => trim($_POST['Country']),
            'Phone' => trim($_POST['Phone']),
            'Birthday' => trim($_POST['Birthday']),
            'TeamId' => (int)$_POST['TeamId']
            
                    ];
                    // validate data
                    // validate title
                    if(empty($data['FirstName'])){
                        $data['FirstName_err'] = 'Please enter a first name';
                    }
                    if(empty($data['LastName'])){
                        $data['LastName_err'] = 'Please enter a last name';
                    }
                    if(empty($data['Email'])){
                        $data['Email_err'] = 'Please enter an email address';
                    }
                    if(empty($data['Address1'])){
                        $data['Address1_err'] = 'Please enter an address';
                    }
                    if(empty($data['Address2'])){
                        $data['Address2_err'] = 'Please enter an address';
                    }
                    if(empty($data['PostalCode'])){
                        $data['PostalCode_err'] = 'Please enter a postal code';
                    }
                    if(empty($data['City'])){
                        $data['City_err'] = 'Please enter a city';
                    }
                    if(empty($data['Country'])){
                        $data['Country_err'] = 'Please enter a country)';
                    }
                    if(empty($data['Phone'])){
                        $data['Phone_err'] = 'Please enter a phone number)';
                    }
                    if(empty($data['Birthday'])){
                        $data['Birthday_err'] = 'Please enter a birthday)';
                    }
                 

                    // make sure there are no errors  
                    if(empty($data['FirstName_err']) && empty($data['LastName_err']) && empty($data['Email_err']) && empty($data['Address1_err']) && empty($data['Address2_err']) && empty($data['PostalCode_err']) && empty($data['City_err'])&& empty($data['Country_err']) && empty($data['Phone_err']) && empty($data['Birthday_err']) ){
                        // validated
                        if($this->playerModel->updatePlayer($data)){
                            
                            // hier is not iets niet juist...
                            flash('post_message', 'Player Updated');
                            redirect('player'); 

                        }else{
                            die('Something went wrong');
                        }

                    }else {
                        // load the view with errors
                        $this->view('competition/player/editPlayer', $data);
                    }

    } else{

        // get the existing player from the model
        $player = $this->playerModel->getPlayerById($id);



        // Check if the logged in user is the owner of the post.. if it's not, then we'll redirect the user away from the edit form
        // if($post->user_id != $_SESSION['user_id']){
        //     redirect('posts');
        // }

        $teams = $this->teamModel->getTeams();
        // hier gaan we de waardes meegeven aan onze view door de variable $data op te vullen
        $data = [
    'FirstName' =>  $player->FirstName,
    'LastName' =>$player->LastName,
    'Email' =>  $player->Email,
    'Address1' =>  $player->Address1,
    'Address2' => $player->Address2,
    'PostalCode' => $player->PostalCode,
    'City' => $player->City,
    'Country' => $player->Country,
    'Phone' => $player->Phone,
    'Birthday' => $player->Birthday,
    'Id' => $player->Id,
    'TeamId' => $player->TeamId,
    'teams' => $teams

            
        ];

        // we nemen dus de id etc..  en geven die mee via $data aan de edit view
        // dit wordt dan samen aan de gebruiker getoond via de browser door de controler.
        $this->view('competition/player/editPlayer', $data);
    }
}





public function deletePlayer($id){
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        // get the existing player from the model
        $player = $this->playerModel->getPlayerById($id);
        // Check if the logged in user is the owner of the post.. if it's not, then we'll redirect the user away from the edit form
        // if($player->user_id != $_SESSION['user_id']){
        //    redirect('footballs');
        // }
        if($this->playerModel->deletePlayer($id)){
            flash('post_message', 'Player Removed');
            redirect('posts');
        }else {
            die('Something went wrong');
        }
    }else {
        redirect('posts');
    }

}

public function searchPlayer(){
    //if($_SERVER['REQUEST_METHOD'] == 'POST'){
// if (isset($_POST['submit'])){

// $player = $this->playerModel->getPlayerByPostalCode($id);

// $data = [
//     'PostalCode' => $player->PostalCode
// ];


$this->view('competition/player/searchPlayer');
   
}

// work in progress
public function searchPlayer_TODO(){
        // Check of het om een POST actie gaat
        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            // Init data
            $data =[
                'email' => trim($_POST['email'])
            ];
// validate email 
if(empty($data['email'])){
    $data['email_err'] = 'Please enter email';
} else {
// check email
if($this->userModel->findUserByEmail($data['email'])){
    $data['email_err'] = 'I found a player with this email.';
}
}

if(!empty($data['email_err'])){
    //Validate


    $this->view('player/searchPlayer', $data);


}
        }
    }

  }