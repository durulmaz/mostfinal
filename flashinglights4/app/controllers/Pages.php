<?php
  class Pages extends Controller {
    public function __construct(){
       

    }

    public function index(){

        if(isLoggedIn()){
            redirect('posts');
        }
        else{  
            redirect('users/login');
            
        }

       
        $data = [
            'title' => 'SportGeeks',
            'description' => 'You sport? you geek? you SportGeek! <br><br> Built on the Kimify MVC PHP framework'
                ];

        $this->view('pages/index', $data);
      
    }

    public function about(){
        $data = ['title' => 'About us',
        'description' => 'Sports updates brought to you by geeks who know very little about sports'];

        $this->view('pages/about', $data);
      
    }
    public function home(){
        
        $data = ['title' => 'Home page',
        'description' => 'Sports updates'];

        $this->view('pages/home', $data);
    }



      
  }