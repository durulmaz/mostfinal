<?php
class Game {
    private $db;

    public function  __construct(){
        $this->db = new Database;
    }

    public function getGames(){
        $this->db->query('SELECT * FROM Game');
    
        $results = $this->db->resultSet();
    
        return $results;
    
    }

    public function insertingone($data){
        $this->db->query('INSERT INTO Game (Date, Status, ScoreHome, ScoreVisitors ) VALUES(:Date, :Status, :ScoreHome, :ScoreVisitors)');
        // Bind values
        $this->db->bind(':Date', $data['Date']);
        $this->db->bind(':Status', $data['Status']);
        $this->db->bind(':ScoreHome', $data['ScoreHome']);
        $this->db->bind(':ScoreVisitors', $data['ScoreVisitors']);

    // Execute
    if($this->db->execute()){
    return true;
    } else {
        return false;
    }
    
}




public function getGameById($id){ 
    $this->db->query('SELECT * FROM Game WHERE Id = :Id');
    $this->db->bind(':Id', $id);

    $row = $this->db->single();

    return $row;


}

public function updateGame($data){
        
    $this->db->query('UPDATE Game SET Date = :Date, Status = :Status, ScoreHome= :ScoreHome, ScoreVisitors = :ScoreVisitors WHERE Id = :Id');
    // Bind values
    $this->db->bind(':Id', $data['Id']);
    $this->db->bind(':Date', $data['Date']);
    $this->db->bind(':Status', $data['Status']);
    $this->db->bind(':ScoreHome', $data['ScoreHome']);
    $this->db->bind(':ScoreVisitors', $data['ScoreVisitors']);

// Execute
if($this->db->execute()){
return true;
} else {
    return false;
}
}


public function deleteGame($id){
            
        
    $this->db->query('DELETE FROM Game WHERE Id = :Id');
    // Bind values
    $this->db->bind(':Id', $id);

    // Execute
    if($this->db->execute()){
return true;
} else {
    return false;
}
    
}



}